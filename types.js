"use strict";
exports.__esModule = true;
var GaugeAnimationData = /** @class */ (function () {
    function GaugeAnimationData(type, size, value, color) {
        this.currentAnimationValue = 0;
        this.color = color;
        this.size = size;
        this.value = value;
        this.type = type;
    }
    return GaugeAnimationData;
}());
exports.GaugeAnimationData = GaugeAnimationData;
// Bar should have a font for display
// indicator should have a polygon
