export class GaugeAnimationData {
    public type: string;
    public color: string;
    public size: number
    public value: number;
    public currentAnimationValue: number = 0;

    constructor(type: string, size: number, value: number, color: string) {
        this.color = color;
        this.size = size;
        this.value = value;
        this.type = type;
    }
}

// Bar should have a font for display and a title.
// indicator should have a polygon