//mport {GaugeAnimationData} from './types';
window.onload = function () {
    //canvas initialization
    var canvas = document.getElementById("canvas");
    var context = canvas.getContext("2d");
    //dimensions
    var W = canvas.width;
    var H = canvas.height;
    //variables
    var text;
    var done_animating;
    var maxValue = 10;
    var minValue = 0;
    var startDegree = 150;
    var endDegree = 30;
    var gaugeDegrees = 360 - startDegree + endDegree; // degrees from startDegree to endDegree
    var barWidth = 10;
    // offset for text
    var offset = 30;
    // Value sets (Per Animated Item)
    var values = [2.5, 6.598, 6.049];
    var colors = ['#77C138', '#75BE36', '#428CC4'];
    var currentPercents = [0, 0, 0];
    var sizes = [190, 140, 120];
    var types = ['indicator', 'bar', 'bar'];
    var titles = ['', 'FA Tractors', 'Other Tractors'];
    var GaugeAnimationData = /** @class */ (function () {
        function GaugeAnimationData(type, radius, value, color, title) {
            this.currentAnimationDegree = 0;
            this.color = color;
            this.radius = radius;
            this.value = value;
            this.type = type;
            this.title = title;
        }
        return GaugeAnimationData;
    }());
    var animations = [];
    for (var i = 0; i < 3; i++) {
        animations.push(new GaugeAnimationData(types[i], sizes[i], values[i], colors[i], titles[i]));
    }
    var barAnimations = animations.filter(function (animation) { return animation.type == 'bar'; });
    var animation_loop, redraw_loop;
    /*
        Main Draw Functions
    */
    // Draw an arc with some options
    function drawArc(radius, width, color, startDegree, endDegree, dashArray, lineCap, blur) {
        if (dashArray === void 0) { dashArray = []; }
        if (lineCap === void 0) { lineCap = "butt"; }
        if (blur === void 0) { blur = false; }
        // Background 360 degree arc
        context.beginPath();
        if (dashArray) {
            context.setLineDash(dashArray);
        }
        if (lineCap) {
            context.lineCap = lineCap;
        }
        if (blur) {
            context.shadowBlur = 5;
            context.shadowColor = 'grey';
        }
        context.strokeStyle = color;
        context.lineWidth = width;
        // context.arc( x, y, radius, startAngle,endAngle [, anticlockwize] );
        context.arc(W / 2, H / 2, radius, toRadians(startDegree), toRadians(endDegree), false); //you can see the arc now
        context.stroke();
        // Reset values
        context.shadowBlur = 0;
    }
    // Draw a Polygon from a set of points
    function drawPolygon(matrix, color) {
        context.beginPath();
        context.strokeStyle = color;
        context.fillStyle = color;
        context.moveTo(matrix[0][0], matrix[0][1]);
        for (var i = 1; i < matrix.length; i++) {
            context.lineTo(matrix[i][0], matrix[i][1]);
        }
        context.fill();
    }
    // Place Text
    function drawText(text, color, font, x, y, centered) {
        if (centered === void 0) { centered = true; }
        context.fillStyle = color;
        context.font = font;
        var text_width = context.measureText(text).width;
        if (centered) {
            x -= text_width / 2;
        }
        context.fillText(text, x, y);
        return text_width;
    }
    function drawLine(x1, x2, y, color, width) {
        context.beginPath();
        context.strokeStyle = color;
        context.lineWidth = width;
        context.moveTo(x1, y);
        context.lineTo(x2, y);
        context.stroke();
    }
    /*
        Helper Functions
    */
    // Convert from degrees to radians
    function toRadians(degree) {
        return degree * (Math.PI / 180);
    }
    // Map input rang to a degree value on the arc (optional use different min max values to map with, default uses object defined values)
    // , minValue?: number, maxValue?: number, startDegree?: number, endDegree?: number
    function mapValueToArc(value) {
        var scale = (gaugeDegrees) / (maxValue - minValue);
        return scale * (value - minValue);
    }
    // Map arc degree to value range
    function mapArcToValue(arcDegree) {
        var scale = (maxValue - minValue) / gaugeDegrees;
        return scale * arcDegree + minValue;
    }
    // Apply a rotation to a polygon
    function rotatePolygon(matrix, degrees) {
        var rotatedMatrix = [];
        var radians = toRadians(degrees);
        matrix.forEach(function (point) {
            var x = point[0], y = point[1];
            var rotated_x = x * Math.cos(radians) - y * Math.sin(radians), rotated_y = x * Math.sin(radians) + y * Math.cos(radians);
            rotatedMatrix.push([rotated_x, rotated_y]);
        });
        return rotatedMatrix;
    }
    // Apply a translation to a polygon
    function translatePolygon(matrix, x, y) {
        matrix.forEach(function (point) {
            point[0] += x;
            point[1] += y;
        });
        return matrix;
    }
    // note: getting draw counts to line up is not as trivial as picking multiples
    // example with [1,5] and [1, 10]:
    // -OOOOO-OOOOO-OOOOO-
    // -0000000000-0000000000-
    /*
        Entry Point
    */
    function scene() {
        // Clear the canvas everytime a chart is drawn
        context.clearRect(0, 0, W, H);
        // Draw tick-marked arcs
        var radius = 200;
        var arcLength = toRadians(240) * radius; // needs to be made dynamic
        var totalMarks = 65; // this should be 60, but 65 works... TODO: figure out why
        var majorMarkPeriod = 5; // 0, 5, 10, 15, 20
        drawArc(radius, 10, 'lightgrey', startDegree, endDegree, [1, arcLength / totalMarks]);
        drawArc(radius, 15, 'grey', startDegree, endDegree, [1, arcLength / (totalMarks / majorMarkPeriod) + majorMarkPeriod - 1]);
        // Draw other static arcs
        drawArc(120, 1, "lightgrey", startDegree, endDegree, []);
        done_animating = true;
        // Draw animated arcs and tic marks
        animations.forEach(function (animation) {
            if (animation.currentAnimationDegree < mapValueToArc(animation.value)) {
                animation.currentAnimationDegree++;
                done_animating = false;
            }
            // handle arcs
            if (animation.type == 'bar') {
                drawArc(animation.radius, barWidth, animation.color, startDegree, startDegree + animation.currentAnimationDegree, [], "butt", true);
            }
            // handle triangle indicator
            else if (animation.type == 'indicator') {
                var radius_1 = animation.radius, length_1 = 15, width = 10; // move this to animation object
                var triangleMatrix = [
                    [radius_1, 0],
                    [(radius_1 - length_1), -(width / 2)],
                    [(radius_1 - length_1), (width / 2)]
                ];
                var displayTriangle = rotatePolygon(triangleMatrix, startDegree + animation.currentAnimationDegree);
                displayTriangle = translatePolygon(displayTriangle, W / 2, H / 2);
                drawPolygon(displayTriangle, animation.color);
            }
        });
        // Text
        var labelCount = barAnimations.length; // TODO: figure out how to use this to adjust the font size.
        var count = 0;
        barAnimations.forEach(function (animation) {
            // Prepare variables
            var fontSize = 50;
            var paddedSize = fontSize + 20;
            var height = paddedSize * labelCount;
            var x = W / 2;
            var y = H / 2 - (height / 2) + offset;
            y += paddedSize * count;
            var text = mapArcToValue(animation.currentAnimationDegree).toFixed(3).toString();
            // Ensure numbers are acurate at end of animation
            if (done_animating) {
                text = animation.value.toFixed(3).toString();
            }
            // Value Display
            var text_width = drawText(text, 'black', 'bold ' + fontSize + 'px Oswald', x, y);
            // Indicator Triangle
            var length = 10, width = 10; // move this to animation object
            var triangleMatrix = [
                [length, 0],
                [0, -(width / 2)],
                [0, (width / 2)]
            ];
            var indicatorTriangle = rotatePolygon(triangleMatrix, 90);
            indicatorTriangle = translatePolygon(indicatorTriangle, x + text_width / 2 + 10, y - fontSize / 4);
            drawPolygon(indicatorTriangle, '#FFC200');
            // Bar Label Color
            var bar_start = x - text_width / 2;
            var bar_end = bar_start + 20;
            var label_height = y + 12;
            drawLine(bar_start, bar_end, y + 12, animation.color, 5);
            // Bar Label Text
            drawText(animation.title, 'grey', 10 + 'px Lato', bar_end + 10, label_height + 3, false);
            count++;
        });
        //Stop animation loop once animations have completed
        if (done_animating) {
            clearInterval(animation_loop);
        }
    }
    function draw() {
        animation_loop = setInterval(scene, 1000 / gaugeDegrees);
    }
    //function to make the chart move to new degrees
    function animate_to() {
        scene();
    }
    // Kick off Animation
    draw();
    //redraw_loop = setInterval(draw, 2000); //Draw a new chart every 2 seconds
};
//# sourceMappingURL=gauge.js.map