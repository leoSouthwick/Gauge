//mport {GaugeAnimationData} from './types';
window.onload = function(){
	//canvas initialization
	let canvas: HTMLCanvasElement = <HTMLCanvasElement> document.getElementById("canvas");
	let context: CanvasRenderingContext2D = canvas.getContext("2d");
	//dimensions
	let W = canvas.width;
	let H = canvas.height;
	
	//variables
	let text;
	let done_animating: boolean;

	let maxValue = 10;
	let minValue = 0;


	let startDegree = 150;
	let endDegree = 30;
	let gaugeDegrees = 360 - startDegree + endDegree; // degrees from startDegree to endDegree
	let barWidth = 10;

	// offset for text
	let offset = 30;

	// Value sets (Per Animated Item)
	let values = [2.5, 6.598, 6.049];
	let colors = ['#77C138', '#75BE36', '#428CC4'];
	let currentPercents = [0, 0, 0];
	let sizes = [190, 140, 120];
	let types = ['indicator', 'bar', 'bar'];
	let titles = ['', 'FA Tractors', 'Other Tractors'];

	class GaugeAnimationData {
		public type: string;
		public color: string;
		public radius: number
		public value: number;
		public title: string;
		public currentAnimationDegree: number = 0;
	
		constructor(type: string, radius: number, value: number, color: string, title: string) {
			this.color = color;
			this.radius = radius;
			this.value = value;
			this.type = type;
			this.title = title;
		}
	}

	let animations: GaugeAnimationData[] = [];

	for(let i = 0; i < 3; i++){
		animations.push(new GaugeAnimationData(types[i], sizes[i], values[i], colors[i], titles[i]));
	}

	var barAnimations = animations.filter(animation => { return animation.type == 'bar'; } );

	let animation_loop, redraw_loop;
	

	/*
		Main Draw Functions
	*/

	// Draw an arc with some options
	function drawArc(radius: number , width: number, color: string, startDegree: number, endDegree: number,  dashArray: number[] = [], lineCap: string = "butt", blur: boolean = false)
	{	
		// Background 360 degree arc
		context.beginPath();
		if(dashArray){
			context.setLineDash(dashArray);
		}	
		if(lineCap){
			context.lineCap=lineCap;
		}
		if(blur){
			context.shadowBlur = 5;
			context.shadowColor = 'grey';
		}
		context.strokeStyle = color;
		context.lineWidth = width;
		// context.arc( x, y, radius, startAngle,endAngle [, anticlockwize] );
		context.arc(W/2, H/2, radius, toRadians(startDegree) , toRadians(endDegree), false); //you can see the arc now
		context.stroke();

		// Reset values
		context.shadowBlur = 0;
	}

	// Draw a Polygon from a set of points
	function drawPolygon(matrix: number[][], color: string) {
		context.beginPath();
		context.strokeStyle = color;
		context.fillStyle = color;
		context.moveTo(matrix[0][0], matrix[0][1]);
		for(let i = 1; i < matrix.length; i++) {
			context.lineTo(matrix[i][0], matrix[i][1]);
		}
		context.fill();
	}

	// Place Text
	function drawText(text: string, color: string, font: string, x: number, y: number, centered: boolean = true) {
		context.fillStyle = color;
		context.font = font;
		let text_width = context.measureText(text).width;
		if(centered) {
			x -= text_width/2
		}
		
		context.fillText(text, x, y);

		return text_width;
	}

	function drawLine(x1: number, x2: number, y: number, color: string, width: number) {
		context.beginPath();
		context.strokeStyle = color;
		context.lineWidth = width;
		context.moveTo(x1, y);
		context.lineTo(x2, y);
		context.stroke();
	}


	/*
		Helper Functions
	*/

	// Convert from degrees to radians
	function toRadians(degree: number) {
		return degree * (Math.PI / 180);
	}

	// Map input rang to a degree value on the arc (optional use different min max values to map with, default uses object defined values)
	// , minValue?: number, maxValue?: number, startDegree?: number, endDegree?: number
	function mapValueToArc(value: number) {
		let scale = (gaugeDegrees) / (maxValue - minValue);
		return scale * (value - minValue);
	}

	// Map arc degree to value range
	function mapArcToValue(arcDegree) {
		let scale = (maxValue - minValue) / gaugeDegrees;
		return scale * arcDegree + minValue;
	}

	// Apply a rotation to a polygon
	function rotatePolygon(matrix: number[][], degrees: number) {
		let rotatedMatrix = [];
		let radians = toRadians(degrees);
		matrix.forEach(point => {
			let x = point[0], 
				y = point[1];
			let rotated_x = x * Math.cos(radians) - y * Math.sin(radians),
				rotated_y = x * Math.sin(radians) + y * Math.cos(radians);
			rotatedMatrix.push([rotated_x, rotated_y]);
		});
		return rotatedMatrix;
	}

	// Apply a translation to a polygon
	function translatePolygon(matrix: number[][], x, y) {
		matrix.forEach(point => {
			point[0] += x;
			point[1] += y;
		});
		return matrix;
	}

	
	
	// note: getting draw counts to line up is not as trivial as picking multiples
	// example with [1,5] and [1, 10]:
	// -OOOOO-OOOOO-OOOOO-
	// -0000000000-0000000000-

	/*
		Entry Point
	*/
	function scene () {
		// Clear the canvas everytime a chart is drawn
		context.clearRect(0, 0, W, H);

		// Draw tick-marked arcs
		let radius = 200;
		let arcLength = toRadians(240) * radius; // needs to be made dynamic
		let totalMarks = 65; // this should be 60, but 65 works... TODO: figure out why
		let majorMarkPeriod = 5; // 0, 5, 10, 15, 20
		drawArc( radius, 10, 'lightgrey', startDegree, endDegree, [1, arcLength/totalMarks]);
		drawArc( radius, 15, 'grey', startDegree, endDegree, [1, arcLength/(totalMarks/majorMarkPeriod) + majorMarkPeriod - 1] );

		// Draw other static arcs
		drawArc( 120, 1, "lightgrey", startDegree, endDegree, []);

		done_animating = true;
		// Draw animated arcs and tic marks
		animations.forEach(animation => {
			if(animation.currentAnimationDegree < mapValueToArc(animation.value) ) 
			{ 
				animation.currentAnimationDegree ++; 
				done_animating = false;
			}

			// handle arcs
			if(animation.type == 'bar'){
				drawArc(animation.radius, barWidth, animation.color, startDegree, startDegree + animation.currentAnimationDegree, [], "butt", true);
			}
			// handle triangle indicator
			else if(animation.type == 'indicator') {
				let radius = animation.radius, length = 15, width = 10; // move this to animation object
				let triangleMatrix = [
					[radius, 0],
					[(radius - length), -(width/2)],
					[(radius - length), (width/2)]
				];
				let displayTriangle = rotatePolygon(triangleMatrix, startDegree + animation.currentAnimationDegree);
				displayTriangle = translatePolygon(displayTriangle, W/2, H/2);
				drawPolygon(displayTriangle, animation.color);
			}
		});
		



		// Text
		let labelCount = barAnimations.length; // TODO: figure out how to use this to adjust the font size.
		let count = 0;

		barAnimations.forEach(animation => {
			// Prepare variables
			let fontSize = 50;
			let paddedSize = fontSize + 20;
			let height = paddedSize * labelCount;
			let x = W/2;
			let y = H/2 - (height/2) + offset;
			y += paddedSize * count;
			let text = mapArcToValue(animation.currentAnimationDegree).toFixed(3).toString()
			// Ensure numbers are acurate at end of animation
			if(done_animating) {
				text = animation.value.toFixed(3).toString();
			}

			// Value Display
			let text_width = drawText(text, 'black', 'bold ' + fontSize + 'px Oswald', x, y);

			// Indicator Triangle
			let length = 10, width = 10; // move this to animation object
			let triangleMatrix = [
				[length, 0],
				[0, -(width/2)],
				[0, (width/2)]
			];
			let indicatorTriangle = rotatePolygon(triangleMatrix, 90);
			indicatorTriangle = translatePolygon(indicatorTriangle, x + text_width/2 + 10, y - fontSize/4);
			drawPolygon(indicatorTriangle, '#FFC200');

			// Bar Label Color
			let bar_start = x - text_width/2;
			let bar_end = bar_start + 20;
			let label_height = y + 12;
			drawLine(bar_start, bar_end, y + 12, animation.color, 5);

			// Bar Label Text
			drawText(animation.title, 'grey', 10 + 'px Lato', bar_end + 10, label_height + 3, false);

			count ++;
		});

		//Stop animation loop once animations have completed
		if(done_animating) {
			clearInterval(animation_loop);
		}
	}
	function draw()
	{
		

		animation_loop = setInterval(scene, 1000/gaugeDegrees);
	}
	


	//function to make the chart move to new degrees
	function animate_to()
	{
		scene();
	}
	
	// Kick off Animation
	draw();
	//redraw_loop = setInterval(draw, 2000); //Draw a new chart every 2 seconds
	
	
	
	
}